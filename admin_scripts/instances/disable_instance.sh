#!/usr/bin/env bash

disable_crontab() {
    echo "[DISABLE-${NAME}] Deleting crontab entry"
    crontab -l | sed "/\( ${NAME} \|\/${NAME}\/\)/d" | crontab -
}

disable_apache() {
    echo "[DISABLE-${NAME}] Deleting apache conf entry"
    if sudo sed -i "/\/${NAME}\//,+14d" ${APACHECONFFILE}; then
        sudo service apache2 restart
    else
        read -p "[DISABLE-${NAME}] Can't sudo: ask an admin to delete '${NAME}' from '${APACHECONFFILE}' and restart apache"
    fi
}

# script starts here
if [[ $# -ne 1 ]]; then
	echo "Usage: $0 instance_name"
	exit 1
fi

# vars
THISSCRIPT=$(readlink -f ${BASH_SOURCE})
THISSCRIPTDIR=$(dirname ${THISSCRIPT})
. ${THISSCRIPTDIR}/config.sh
NAME=$1

# main
disable_crontab
disable_apache
