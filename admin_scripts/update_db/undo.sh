#!/bin/bash

psql -U postgres <<< "DROP DATABASE pcrs108; CREATE DATABASE pcrs108;"
psql -U postgres pcrs108 <<< "\i ../../doc/images/108.psql"
