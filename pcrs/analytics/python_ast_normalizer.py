"""
Usage:
  normalized_ast.py <code.py>

  The program expects the name of a python file to be parsed into a
  normalized AST.
"""
import sys
import ast
import astunparse
from collections import OrderedDict


class NormalizerVisitor(ast.NodeVisitor):
    '''Normalizes the AST being visited by replacing variable names with canonical names.
    '''

    VARNAME = 'name{}'

    def __init__(self):
        self.varindex = 0
        self.varmap = {} # The use of a single environment means that repeated names are given the same canonical name

    def _get_canonical_name(self, name):
        print(name)
        if name not in self.varmap:
            self.varmap[name] = self.VARNAME.format(self.varindex)
            self.varindex += 1
        print(self.varmap[name], self.varindex)
        return self.varmap[name]

    def visit_arg(self, node):
        # arg -> str name
        # annotation -> None???
        node.arg = self._get_canonical_name(node.arg)
        super().generic_visit(node)

    def visit_Call(self, node):
        # func -> Name

        # Manually invoking visit, to avoid visiting "func" if it is a builtin
        if not hasattr(__builtins__, node.func.id):
            self.visit(node.func)
        for n in node.args:
            self.visit(n)
        for n in node.keywords:
            self.visit(n)
        if node.starargs:
            for n in node.starargs:
                self.visit(n)
        if node.kwargs:
            for n in node.kwargs:
                self.visit(n)

    def visit_ClassDef(self, node):
        # TODO: rename class names and constructor calls? (node.name)
        super().generic_visit(node)

    def visit_For(self, node):
        # Listed in case loop targets need replacement ...
        # target -> loop variable name
        super().generic_visit(node)

    def visit_FunctionDef(self, node):
        # If we were doing name replacement respecting namespaces, we'd need to push and pop a namespace here.
        # name -> str name
        node.name = self._get_canonical_name(node.name)
        super().generic_visit(node)

    def visit_keyword(self, node):
        # Listed in case keywords need replacement ...
        # arg -> str keyword name
        # value -> value of argument
        super().generic_visit(node)

    def visit_Name(self, node):
        # id -> str
        # ctx -> Load/Store object (memory location?)
        node.id = self._get_canonical_name(node.id)
        super().generic_visit(node)

if __name__ == '__main__':
    import docopt
    arguments = docopt.docopt(__doc__)
    fname = arguments.get('<code.py>')

    try:
        code = open(fname).read()
    except IOError:
        print('Could not open {0}'.format(fname), file=sys.stderr)
        sys.exit()

    # Emits normalized AST to stdout
    tree = ast.parse(code)
    NormalizerVisitor().visit(tree)
    print(astunparse.unparse(tree))