# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2017-08-25 16:07
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('content', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='MasteryQuizChallenge',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('quest', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='content.Quest')),
                ('requires', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizChallenge')),
            ],
        ),
        migrations.CreateModel(
            name='MasteryQuizSession',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.CharField(max_length=12)),
                ('time_started', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('time_ended', models.DateTimeField(null=True)),
                ('started_by', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='MasteryQuizSessionParticipant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('time_joined', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('mastery_quiz_challenge', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizChallenge')),
                ('mastery_quiz_session', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizSession')),
            ],
        ),
        migrations.CreateModel(
            name='MasteryQuizTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255, null=True)),
                ('out_of', models.SmallIntegerField()),
                ('pass_threshold', models.FloatField()),
                ('mastery_quiz_challenge', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizChallenge')),
            ],
        ),
        migrations.CreateModel(
            name='MasteryQuizTestSubmission',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('score', models.SmallIntegerField()),
                ('date_marked', models.DateTimeField(blank=True, default=django.utils.timezone.now)),
                ('mastery_quiz_test', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizTest')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='masteryquizsessionparticipant',
            name='mastery_quiz_test',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mastery.MasteryQuizTest'),
        ),
        migrations.AddField(
            model_name='masteryquizsessionparticipant',
            name='user',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
